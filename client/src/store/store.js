import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas';
import { composeWithDevTools } from 'redux-devtools-extension';

import chatReducer from './Chat/reducer';
import profileReducer from './Login/reducer';
import registrationReducer from './Registration/reducer';
export const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware()

const initialState = {};

const middlewares = [thunk, routerMiddleware(history), sagaMiddleware];

const composedEnhancers = compose(
  composeWithDevTools(applyMiddleware(...middlewares))
);

const reducers = {
  messages: chatReducer,
  profile: profileReducer,
  registration: registrationReducer
};

const rootReducer = combineReducers({
  router: connectRouter(history),
  ...reducers,
});


const store = createStore(rootReducer, initialState, composedEnhancers);

sagaMiddleware.run(rootSaga)

export default store;
