import { all } from 'redux-saga/effects'
import watchLogin from './Login/sagas';
import watchRegistration from './Registration/sagas';


export default function* rootSaga() {
  yield all([
    watchLogin(),
    watchRegistration()
  ])
}
