
import {
  SET_LOGIN_VALUE,
  SET_PASSWORD_VALUE,
  SET_LOGIN_LOADING_TOGGLE,
  CALL_LOGIN,
} from './actionTypes';

const initialState = {
  login:"",
  password:"",
  loginBtnLoading:false,
  isAuthorized:false,
  user:{}
}
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN_VALUE:
      return {
        ...state,
        login: action.login,
      };
    case SET_PASSWORD_VALUE:
      return {
        ...state,
        password: action.password,
      };
    case SET_LOGIN_LOADING_TOGGLE:
      return {
        ...state,
        loginBtnLoading: action.loginBtnLoading,
      };
    case CALL_LOGIN:
      console.log(action)
      return {
        ...state,
        user: action.loginData.data.user,
        isAuthorized: Boolean(action.loginData.data.user?.id),
      };
    default:
      return state;
  }
};
