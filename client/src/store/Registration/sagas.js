import { takeEvery, call, put, select } from "redux-saga/effects";
import {setRegistrationLoadingToggleAction, setRegistrationDataAction} from './actions';
import {setLoginDataAction} from '../Login/actions';
import {REGISTRATION} from './actionTypes';
import { registrationApiCall } from "../../services/authService";

function* registrationCall() {
  yield put(setRegistrationLoadingToggleAction(true));

  const { registration } = yield select()

  const login = registration.login;
  const username = registration.username;
  const password = registration.password;
  try {
     const userAuthData = yield call(registrationApiCall,{login, username, password});
     console.log(userAuthData);
    const token = userAuthData.data.token;
    localStorage.setItem('token', token);
    yield put(setLoginDataAction(userAuthData))
  }
  catch(err) {
    console.log(err);
  }

  yield put(setRegistrationLoadingToggleAction(false));
}

export default function* watchRegistration() {
    yield takeEvery(REGISTRATION, registrationCall)
}
