import {
  SET_REGISTRATION_LOGIN_VALUE,
  SET_REGISTRATION_USERNAME_VALUE,
  SET_REGISTRATION_PASSWORD_VALUE,
  SET_REGISTRATION_LOADING_TOGGLE,
  CALL_REGISTRATION,
  REGISTRATION
} from "./actionTypes";

export const setRegistrationLoginAction = (registrationLogin) => ({
  type: SET_REGISTRATION_LOGIN_VALUE,
  registrationLogin,
});

export const setRegistrationUsernameAction = (registrationUsername) => ({
  type: SET_REGISTRATION_USERNAME_VALUE,
  registrationUsername,
});

export const setRegistrationPasswordAction = (registrationPassword) => ({
  type: SET_REGISTRATION_PASSWORD_VALUE,
  registrationPassword,
});

export const setRegistrationLoadingToggleAction = (registrationBtnLoading) => ({
  type: SET_REGISTRATION_LOADING_TOGGLE,
  registrationBtnLoading,
});

export const setRegistrationDataAction = (registrationData) => ({
  type: CALL_REGISTRATION,
  registrationData,
});

export const registrationAction = (registrationData) => ({
  type: REGISTRATION,

});

