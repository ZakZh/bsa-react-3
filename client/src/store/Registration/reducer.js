
import {
  SET_REGISTRATION_LOGIN_VALUE,
  SET_REGISTRATION_USERNAME_VALUE,
  SET_REGISTRATION_PASSWORD_VALUE,
  SET_REGISTRATION_LOADING_TOGGLE,
  CALL_REGISTRATION
} from './actionTypes';

const initialState = {
  login:"",
  username:"",
  password:"",
  registrationBtnLoading:false,
  user:{},
  isAuthorized:false
}
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action) => {
  switch (action.type) {
    case SET_REGISTRATION_LOGIN_VALUE:
      return {
        ...state,
        login: action.registrationLogin,
      };
    case SET_REGISTRATION_USERNAME_VALUE:
      return {
        ...state,
        username: action.registrationUsername,
      };
    case SET_REGISTRATION_PASSWORD_VALUE:
      return {
        ...state,
        password: action.registrationPassword,
      };
    case SET_REGISTRATION_LOADING_TOGGLE:
      return {
        ...state,
        registrationBtnLoading: action.registrationBtnLoading,
      };
    case CALL_REGISTRATION:
      return {
        ...state,
      };
    default:
      return state;
  }
};


