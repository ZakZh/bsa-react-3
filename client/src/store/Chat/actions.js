import {
  SEE_ALL_MESSAGES,
  ADD_MESSAGE,
  TOGGLE_UPDATE_MESSAGE_FORM,
} from "./actionTypes";
import { v4 as uuidv4 } from "uuid";

const setMessagesAction = (messages) => ({
  type: SEE_ALL_MESSAGES,
  messages,
});

const setMessageAction = (message) => ({
  type: ADD_MESSAGE,
  message,
});

const setExpandedUpdateMessageAction = (updatedMessage) => ({
  type: TOGGLE_UPDATE_MESSAGE_FORM,
  updatedMessage,
});

export const toggleUpdateMessageForm = (messageId) => async (
  dispatch,
  getRootState
) => {
  const {
    messages: { messages },
  } = getRootState();
  const updatedMessage = messages.filter((message) =>
    messageId === message.id ? message : undefined
  );
  dispatch(setExpandedUpdateMessageAction(updatedMessage[0]));
};

export const setUpdateMessage = (updateMessageText) => (
  dispatch,
  getRootState
) => {
  const {
    messages: { updatedMessage },
  } = getRootState();

  const updatedMessageText = {
    ...updatedMessage,
    text: updateMessageText,
  };

  dispatch(setExpandedUpdateMessageAction(updatedMessageText));
};

export const updateMessage = () => (dispatch, getRootState) => {
  const {
    messages: { messages, updatedMessage },
  } = getRootState();

  const newMessages = messages.map((message) => {
    if (message.id === updatedMessage.id) {
      return updatedMessage;
    }
    return message;
  });
  dispatch(setMessagesAction(newMessages));
  dispatch(setExpandedUpdateMessageAction());
};

function sortMessages(a, b) {
  if (a.createdAt < b.createdAt) {
    return -1;
  }
  if (a.createdAt > b.createdAt) {
    return 1;
  }
  return 0;
}

export const loadMessages = (dispatch) => {
  fetch("https://edikdolynskyi.github.io/react_sources/messages.json")
    .then((res) => {
      return res.json();
    })
    .then((messages) => {
      dispatch(setMessagesAction(messages.sort(sortMessages)));
    });
};

export const setNewMessageValue = (newMessage) => (dispatch) => {
  dispatch(setMessageAction(newMessage));
};

export const sendMessage = () => (dispatch, getRootState) => {
  const {
    messages: { messages, message },
  } = getRootState();

  let newId = uuidv4();
  let createdAt = new Date().toISOString();
  const editedAt = "";
  const userId = "me";
  const user = "Me";

  const newMessageObject = {
    id: newId,
    text: message,
    createdAt,
    editedAt,
    userId,
    user,
  };
  if (!messages) {
    dispatch(setMessagesAction([newMessageObject]));
  }else {
    const editedMessages = [...messages, newMessageObject];
    dispatch(setMessagesAction(editedMessages));
  }

  dispatch(setMessageAction(""));

};
