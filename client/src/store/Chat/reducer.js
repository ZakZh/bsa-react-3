import {
  SEE_ALL_MESSAGES,
  ADD_MESSAGE,
  TOGGLE_UPDATE_MESSAGE_FORM,
} from "./actionTypes";

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case SEE_ALL_MESSAGES:
      return {
        ...state,
        messages: action.messages,
      };
    case ADD_MESSAGE:
      return {
        ...state,
        message: action.message,
      };
    case TOGGLE_UPDATE_MESSAGE_FORM:
      return {
        ...state,
        updatedMessage: action.updatedMessage,
      };
    default:
      return state;
  }
};
