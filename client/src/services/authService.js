// import axios from "axios";

import API from '../config/axiosConfig'

export const loginApiCall = (request) => API.post(
  '/api/auth/login',
  request
);

export const registrationApiCall = (request) => API.post(
  '/api/auth/registration',
  request
);

export const chatApiCall = (request) => API.post(
  '/api/auth/chat',
  request
);

