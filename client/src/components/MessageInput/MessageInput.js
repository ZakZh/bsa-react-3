import React from "react";
import { Form, TextArea } from "semantic-ui-react";
import PropTypes from "prop-types";

function MessageInput({ sendMessage, setNewMessageValue, message }) {
  return (
    <Form className="">
      <Form.Group>
        <Form.TextArea
          className=""
          id="newMessageTextArea"
          placeholder="Enter your message..."
          value={message}
          onChange={(e) => setNewMessageValue(e.target.value)}
          rows="2"
          width={14}
        />
        <Form.Button
          width={2}
          className=""
          primary
          onClick={() => sendMessage()}
        >
          Send
        </Form.Button>
      </Form.Group>
    </Form>
  );
}

MessageInput.propTypes = {
  sendMessage: PropTypes.func.isRequired,
  setNewMessageValue: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
};

export default MessageInput;
