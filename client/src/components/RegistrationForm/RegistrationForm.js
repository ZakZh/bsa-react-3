import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';

const RegistrationForm = ({
    setRegistrationLoginAction,
    setRegistrationUsernameAction,
    setRegistrationPasswordAction,
    registrationAction,
    login,
    password,
    username,
    registrationBtnLoading
  }) => {

  return (
    <Form name="registrationForm" size="large" onSubmit={()=>{}}>
      <Segment>
        <Form.Input
          fluid
          icon="triangle right"
          iconPosition="left"
          placeholder="Login"
          type="text"
          value={login}
          onChange={ev => {setRegistrationLoginAction(ev.target.value)}}
        />
        <Form.Input
          fluid
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          value={username}
          onChange={ev => {setRegistrationUsernameAction(ev.target.value)}}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          value={password}
          onChange={ev => {setRegistrationPasswordAction(ev.target.value)}}
        />
        <Button
          type="button"
          disabled={registrationBtnLoading}
          loading={registrationBtnLoading}
          onClick={()=>registrationAction()}
          color="teal"
          fluid
          size="large"
          primary
        >
          Register
        </Button>
      </Segment>
    </Form>
  );
};

RegistrationForm.propTypes = {
  setRegistrationLoginAction:PropTypes.func.isRequired,
  setRegistrationUsernameAction:PropTypes.func.isRequired,
  setRegistrationPasswordAction:PropTypes.func.isRequired,
  registrationAction:PropTypes.func.isRequired,
  login: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  registrationBtnLoading:PropTypes.bool.isRequired
};

export default RegistrationForm;
