import React from "react";
import { Comment, Button } from "semantic-ui-react";
import PropTypes from "prop-types";

function MessageList({ messages, toggleUpdateMessageForm }) {
  let messageDate = "";
  const messagesList = messages.map((message) => {
    let currentMessageDate = new Date(message.createdAt).toDateString();
    const messageSeparator =
      currentMessageDate !== messageDate ? (
        <div className="col-12 separator">{currentMessageDate}</div>
      ) : (
        ""
      );

    messageDate = new Date(message.createdAt).toDateString();

    if (message.userId === "me") {
      return (
        <div key={message.id}>
          {messageSeparator}
          <Comment
            className=""
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
              margin: "3px",
            }}
          >
            <Comment.Action>
              <Button
                circular
                icon="settings"
                content="Edit"
                size="mini"
                onClick={() => toggleUpdateMessageForm(message.id)}
              ></Button>
            </Comment.Action>
            <Comment.Content
              key={message.id}
              className=""
              style={{
                backgroundColor: "rgb(91 131 197)",
                borderRadius: "3px",
                padding: "3px",
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-end",
              }}
            >
              <Comment.Author>{message.user}</Comment.Author>

              <Comment.Text className="">{message.text}</Comment.Text>
            </Comment.Content>
          </Comment>
        </div>
      );
    }

    return (
      <div key={message.id}>
        {messageSeparator}
        <Comment
          className=""
          style={{ display: "flex", alignItems: "flex-start" }}
        >
          <Comment.Avatar
            src={message.avatar}
            className=""
            alt={message.user}
          />
          <Comment.Content
            style={{
              backgroundColor: "rgb(179 190 236)",
              borderRadius: "3px",
              width: "",
              maxWidth: "60%",
              padding: "5px",
              margin: "3px",
            }}
          >
            <Comment.Author>{message.user}</Comment.Author>
            <Comment.Text className="">{message.text}</Comment.Text>
          </Comment.Content>
        </Comment>
      </div>
    );
  });

  return (
    <Comment.Group
      className=""
      style={{ maxWidth: "100%", height: "100%", overflowX: "auto" }}
    >
      {messagesList}
    </Comment.Group>
  );
}

export default MessageList;
