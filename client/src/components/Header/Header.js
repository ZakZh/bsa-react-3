import React from "react";
import { Menu } from "semantic-ui-react";
import PropTypes from "prop-types";

function Header({ chatName, userCount, messagesCount, lastMessageTime }) {
  return (
    <Menu className="" attached="top" style={{ backgroundColor: "#3595f5" }}>
      <Menu.Item className="d-inline">
        <Menu.Item className="d-inline m-4">
          <strong>{chatName}</strong>
        </Menu.Item>
        <Menu.Item className="d-inline m-2">
          <strong>{userCount}</strong> participants
        </Menu.Item>
        <Menu.Item className="d-inline m-2">
          <strong>{messagesCount}</strong> messages
        </Menu.Item>
      </Menu.Item>
      <Menu.Item className="" position="right">
        last message: <strong>{lastMessageTime}</strong>
      </Menu.Item>
    </Menu>
  );
}

Header.propTypes = {
  chatName: PropTypes.string.isRequired,
  userCount: PropTypes.number.isRequired,
  messagesCount: PropTypes.number.isRequired,
  lastMessageTime: PropTypes.string.isRequired,
};

export default Header;
