import React, { useState } from 'react';
import { Container, Dimmer, Loader, Segment } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HeaderContainer from '../../components/Header/HeaderContainer';
import MessageListContainer from '../../components/MessageList/MessageListContainer';
import MessageInputContainer from '../../components/MessageInput/MessageInputContainer';
import ModalUpdateMessageContainer from '../../components/ModalUpdateMessage/ModalUpdateMessageContainer';
import './Chat.css';

import {
  loadMessages,
  setNewMessageValue,
  sendMessage,
  toggleUpdateMessageForm,
  setUpdateMessage,
  updateMessage,
} from '../../store/Chat/actions';

function Chat({
  messages = [],
  message,
  sendMessage,
  setNewMessageValue,
  updatedMessage,
  toggleUpdateMessageForm,
  setUpdateMessage,
  updateMessage,
}) {
  const [loading, setLoading] = useState(false);
  const messagesCount = messages.length;
  const userCount = uniqueUsers();
  const lastMessage = messages[messages.length - 1];

  function uniqueUsers() {
    const users = new Set();
    messages.map((message) => {
      users.add(message.userId);
      return message;
    });
    return users.size;
  }

  const chatMeta = {
    chatName: 'Chat',
    messagesCount,
    userCount,
    lastMessageTime: lastMessage
      ? new Date(lastMessage.createdAt).toLocaleString(undefined, {
          day: 'numeric',
          month: 'numeric',
          year: 'numeric',
          hour: '2-digit',
          minute: '2-digit',
        })
      : new Date().toLocaleString(undefined, {
          day: 'numeric',
          month: 'numeric',
          year: 'numeric',
          hour: '2-digit',
          minute: '2-digit',
        }),
  };

  return (
    <>
      {loading ? (
        <Dimmer active inverted>
          <Loader size="massive" inverted />
        </Dimmer>
      ) : (
        <Container className="" style={{ height: '100%' }}>
          <HeaderContainer chatMeta={chatMeta} />
          <Segment attached loading={loading} style={{ height: '89vh' }}>
            <MessageListContainer
              messages={messages}
              toggleUpdateMessageForm={toggleUpdateMessageForm}
            />
          </Segment>
          <MessageInputContainer
            sendMessage={sendMessage}
            setNewMessageValue={setNewMessageValue}
            message={message}
          />
        </Container>
      )}
      {updatedMessage && (
        <ModalUpdateMessageContainer
          toggleUpdateMessageForm={toggleUpdateMessageForm}
          updatedMessage={updatedMessage}
          setUpdateMessage={setUpdateMessage}
          updateMessage={updateMessage}
        />
      )}
    </>
  );
}

Chat.propTypes = {
  message: PropTypes.string.isRequired,
  messages: PropTypes.array.isRequired,
  sendMessage: PropTypes.func.isRequired,
  setNewMessageValue: PropTypes.func.isRequired,
  updatedMessage: PropTypes.objectOf(PropTypes.any),
  loadMessages: PropTypes.func.isRequired,
  toggleUpdateMessageForm: PropTypes.func.isRequired,
  setUpdateMessage: PropTypes.func.isRequired,
  updateMessage: PropTypes.func.isRequired,
};

Chat.defaultProps = {
  messages: [],
  message: '',
  updatedMessage: undefined,
};

const mapStateToProps = (rootState) => ({
  messages: rootState.messages.messages,
  message: rootState.messages.message,
  updatedMessage: rootState.messages.updatedMessage,
});

const actions = {
  loadMessages,
  setNewMessageValue,
  sendMessage,
  toggleUpdateMessageForm,
  setUpdateMessage,
  updateMessage,
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
