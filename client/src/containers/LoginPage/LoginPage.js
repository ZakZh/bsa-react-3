import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import LoginForm from '../../components/LoginForm/LoginForm';

import {
  setLoginAction,
  setPasswordAction,
  loginAction
} from '../../store/Login/actions'

const LoginPage = (
  {
    login,
    password,
    loginBtnLoading,
    setLoginAction,
    setPasswordAction,
    loginAction
  }) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Header as="h2" color="teal" textAlign="center">
        Login to your account
      </Header>
      <LoginForm
        login={login}
        password={password}
        setLoginAction={setLoginAction}
        setPasswordAction={setPasswordAction}
        loginBtnLoading={loginBtnLoading}
        loginAction={loginAction}
      />
      <Message>
        New to us?
        {' '}
        <NavLink exact to="/registration">Sign Up</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

LoginPage.propTypes = {
setLoginAction: PropTypes.func.isRequired,
setPasswordAction: PropTypes.func.isRequired,
loginAction: PropTypes.func.isRequired,
login:PropTypes.string.isRequired,
password:PropTypes.string.isRequired,
loginBtnLoading:PropTypes.bool.isRequired
};

LoginPage.defaultProps ={
  setLoginAction: ()=>{},
  setPasswordAction: ()=>{},
  setLoginDataAction: ()=>{},
}

const actions = {
  setLoginAction,
  setPasswordAction,
  loginAction
};

const mapStateToProps = (rootState) => ({
  login: rootState.profile.login,
  password: rootState.profile.password,
  loginBtnLoading:rootState.profile.loginBtnLoading
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
