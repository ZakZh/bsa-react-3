import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import RegistrationForm from '../../components/RegistrationForm/RegistrationForm';

import {
  setRegistrationLoginAction,
  setRegistrationUsernameAction,
  setRegistrationPasswordAction,
  registrationAction
} from '../../store/Registration/actions'

const RegistrationPage = (
  {
    setRegistrationLoginAction,
    setRegistrationUsernameAction,
    setRegistrationPasswordAction,
    registrationAction,
    login,
    password,
    username,
    registrationBtnLoading
  }) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Header as="h2" color="teal" textAlign="center">
        Register for free account
      </Header>
      <RegistrationForm
        login={login}
        password={password}
        username={username}
        setRegistrationLoginAction={setRegistrationLoginAction}
        setRegistrationUsernameAction={setRegistrationUsernameAction}
        setRegistrationPasswordAction={setRegistrationPasswordAction}
        registrationAction={registrationAction}
        registrationBtnLoading={registrationBtnLoading}
      />
      <Message>
        Alredy with us?
        {' '}
        <NavLink exact to="/login">Sign In</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

RegistrationPage.propTypes = {
  setRegistrationLoginAction:PropTypes.func.isRequired,
  setRegistrationUsernameAction:PropTypes.func.isRequired,
  setRegistrationPasswordAction:PropTypes.func.isRequired,
  registrationAction:PropTypes.func.isRequired,
  login: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  registrationBtnLoading:PropTypes.bool.isRequired
};

RegistrationPage.defaultProps = {
  setRegistrationLoginAction:PropTypes.func.isRequired,
  setRegistrationUsernameAction:PropTypes.func.isRequired,
  setRegistrationPasswordAction:PropTypes.func.isRequired,
  registrationAction:PropTypes.func.isRequired
};

const actions = {
  setRegistrationLoginAction,
  setRegistrationUsernameAction,
  setRegistrationPasswordAction,
  registrationAction
};

const mapStateToProps = (rootState) => ({
  login: rootState.registration.login,
  password: rootState.registration.password,
  username: rootState.registration.username,
  registrationBtnLoading:rootState.registration.registrationBtnLoading
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationPage);
