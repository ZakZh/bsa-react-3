import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Chat from '../Chat/Chat'
import LoginPage from '../LoginPage/LoginPage'
import RegistrationPage from '../RegistrationPage/RegistrationPage'
import PrivateRoute from './PrivateRoute/';
import PublicRoute from './PublicRoute/';

function Router() {
  return (
    <Switch>
      <PublicRoute exact path="/login" component={LoginPage} />
      <PublicRoute exact path="/registration" component={RegistrationPage} />
      <PrivateRoute exact path="/" component={Chat} />
    </Switch>
  )
}

export default Router;
