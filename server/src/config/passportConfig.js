
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { secret } from './jwtConfig';
import { findUserByLogin, findUserById } from '../data/repositories/userRepository';
import { compare } from '../helpers/cryptoHelper';

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: secret,
};

passport.use(
  'login',
  new LocalStrategy({ usernameField: 'login' }, async (login, password, done) => {
    try {
      const user = await findUserByLogin(login);
      if (!user) {
        return done({ status: 401, message: 'Incorrect login.' }, false);
      }

      return await compare(password, user.password)
        ? done(null, user)
        : done({ status: 401, message: 'Passwords do not match.' }, null, false);
    } catch (err) {
      return done(err);
    }
  }),
);

passport.use(
  'register',
  new LocalStrategy(
    { passReqToCallback: true },
    async ({ body: { login } }, username, password, done) => {
      try {
        return await await findUserByLogin(login)
          ? done({ status: 401, message: 'Username is already taken.' }, null)
          : done(null, { login, username, password });
      } catch (err) {
        return done(err);
      }
    },
  ),
);

passport.use(new JwtStrategy(options, async ({ id }, done) => {
  try {
    const user = await findUserById(id);
    return user ? done(null, user) : done({ status: 401, message: 'Token is invalid.' }, null);
  } catch (err) {
    return done(err);
  }
}));
