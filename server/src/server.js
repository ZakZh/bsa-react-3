import express from 'express';
import path from 'path';
import passport from 'passport';
import './config/passportConfig';
import env from './env';
import routes from './api/routes/index';

const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());

routes(app);

const staticPath = path.resolve(`${__dirname}/../../client/public/`);
app.use(express.static(staticPath));

app.get('*', (req, res) => {
  res.redirect('http://localhost:3000/');
});

app.listen(env.app.port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server listening on port ${env.app.port}!`);
});
