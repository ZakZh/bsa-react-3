import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import { addUser, findUserById } from '../../data/repositories/userRepository';

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await findUserById(id),
});

export const register = async ({ password, ...userData }) => {
  const newUser = await addUser({
    ...userData,
    password: await encrypt(password),
  });
  return login(newUser);
};
