import fs from 'fs';
import path from 'path';
import { v4 as uuidv4 } from 'uuid';

export function findUserByLogin(login) {
  const usersJson = fs.readFileSync(path.resolve(__dirname, '../db/users.json'));

  const users = JSON.parse(usersJson);
  const user = users.find(userInArray => userInArray.login === login);
  return (user) || undefined;
}

export async function findUserById(id) {
  const usersJson = await fs.readFileSync(path.resolve(__dirname, '../db/users.json'));

  const users = JSON.parse(usersJson);
  const user = users.find(userInArray => userInArray.id === id);

  return (user) || undefined;
}

export async function addUser(data) {
  const newUser = {
    id: uuidv4(),
    name: data.username,
    password: data.password,
    login: data.login,
    avatar: '',
  };

  const usersJson = await fs.readFileSync(path.resolve(__dirname, '../db/users.json'));

  let users = JSON.parse(usersJson);
  users.push(newUser);

  users = JSON.stringify(users);

  fs.writeFileSync(path.resolve(__dirname, '../db/users.json'), users, 'utf8');

  return findUserById(newUser.id);
}
